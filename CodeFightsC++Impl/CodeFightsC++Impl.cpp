#pragma region Environment
#include "stdafx.h"

#include <iostream>
#include <vector>
#include <cstdarg>
#include <string>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <queue>
#include <map>
#include <set>
#include <algorithm>
#include <climits>
#include <sstream>
#include <numeric>
#include <iterator>
#include <iomanip>
#include <utility>
#include <stack>
#include <functional>
#include <deque>
#include <complex>
#include <bitset>
#include <list>
#include <array>
#include <regex>
#include <unordered_set>
#include <unordered_map>
#pragma endregion

#include <memory>

#pragma region Environment
class ExecutionEnvironment
{
public:
#pragma endregion

//$startcontent
	// --------------------------------------------------------------------------
	// Environment workarounds
	// --------------------------------------------------------------------------

	// This is the required function; it is a hook for the CodeFights execution environment. This is a MEMBER FUNCTION of
	// a hidden class. Recommend treating this as a thin wrapper and implementing the actual task logic in the global
	// environment below to avoid weird restrictions.
	void task()
	{
		// Extern to forward-declare these free functions from within this member function
		extern void RunTests();
		RunTests();
		extern void Task();
		Task();
	}

	/*
	Working around the execution environment here. All of this code will be injected into a class definition that looks
	like:

	class _runxxxxx {
	<code injected here>
	};

	By injecting a close curly brace, we can close the class definition early. This allows us to use proper free functions
	in our solution, and it works around a number of other odd quirks of writing code in this environment.

	For good measure, we'll add a dummy class definition at the bottom to satisfy the final close bracket in the enclosing
	code.
	*/
};

using namespace std;

// --------------------------------------------------------------------------
// Problem Solution
// --------------------------------------------------------------------------

void Task()
{

}

#pragma region testing
// --------------------------------------------------------------------------
// Testing Infrastructure
// --------------------------------------------------------------------------

// Crude testing infrastructure, to keep me sane
class TestSuite
{
public:
	void Run()
	{
		vector<TestCaseResult> testCaseResults;

		auto logResult = [&testCaseResults](string name, Result result, string message = "") {
			testCaseResults.push_back(TestCaseResult(name, result, message));
		};

		for (auto testCase : testCases)
		{
			try
			{
				testCase.run();
				logResult(testCase.name, Result::Succeeded);
			}
			catch (AssertException& assert)
			{
				logResult(testCase.name, Result::Failed, assert.what());
			}
			catch (exception& e)
			{
				logResult(testCase.name, Result::Failed, "unexpected error: " + string(e.what()));
			}
		}

		auto suiteResult = Result::Succeeded;
		ostringstream failureOutput;

		for (const auto& testCaseResult : testCaseResults)
		{
			if (testCaseResult.result == Result::Failed)
			{
				suiteResult = Result::Failed;
				failureOutput << "    " << testCaseResult.name << ": " << testCaseResult.message << endl;
			}
		}

		cout << GetName() << " ";

		if (suiteResult == Result::Succeeded)
		{
			cout << "PASSED" << endl;
		}
		else
		{
			cout << "FAILED" << endl << failureOutput.str();
		}
	}

	enum class Result
	{
		Succeeded,
		Failed
	};

protected:
	virtual string GetName() = 0;

	struct TestCaseResult
	{
		TestCaseResult(string name, Result result, string message)
			: name(name)
			, result(result)
			, message(message)
		{
		}

		string name;
		Result result;
		string message;
	};

	using TestCaseFn = function<void()>;

	struct TestCase
	{
		TestCase(string name, TestCaseFn testCaseFn)
			: name(name)
			, run(testCaseFn)
		{
		}

		string name;
		TestCaseFn run;
	};

	vector<TestCase> testCases;

	void RegisterTestCase(string name, TestCaseFn testCaseFn)
	{
		testCases.push_back(TestCase(name, testCaseFn));
	}

	class AssertException : public runtime_error
	{
	public:
		AssertException(const string& message) : runtime_error(message)
		{}
	};

	void assert(bool condition, string message = "")
	{
		if (!condition)
		{
			if (message.size() == 0)
				message = "Expected true, found false";

			throw AssertException(message);
		}
	}

	template<typename T>
	void assertEqual(T expected, T actual, string message = "")
	{
		if (expected != actual)
		{
			ostringstream stream;
			stream << message << " (expected: " << expected << ", actual: " << actual << ")";
			throw AssertException(stream.str());
		}
	}
};

// --------------------------------------------------------------------------
// Tests
// --------------------------------------------------------------------------

class ExampleTestSuite : public TestSuite
{
public:
	ExampleTestSuite()
	{
		RegisterTestCase("ExampleTestCase", [this] { ExampleTestCase(); });
		RegisterTestCase("ExampleFailingTestCase", [this] { ExampleFailingTestCase(); });
	}

	string GetName() override
	{
		return "ExampleTestSuite";
	}

private:
	void ExampleTestCase()
	{
	}

	void ExampleFailingTestCase()
	{
		assertEqual(1, 2);
	}
};

vector<unique_ptr<TestSuite>> EnumerateTestSuites()
{
	// Can't use initializer lists here because they copy each element, and
	// unique_ptrs are not copyable
	vector<unique_ptr<TestSuite>> tests;
	tests.push_back(make_unique<ExampleTestSuite>());

	return tests;
}

void RunTests()
{
	for (const auto& test : EnumerateTestSuites())
		test->Run();
}
#pragma endregion

// This is the opening portion of a dummy class, as promised in the comment above
class Dummy
{
//$endcontent

#pragma region Environment
};

#include <chrono>

int main()
{
	using Clock = chrono::high_resolution_clock;
	auto start = Clock::now();
	RunTests();
	auto end = Clock::now();
	cout << "Elapsed ms: " << chrono::duration_cast<chrono::milliseconds>(end - start).count() << endl;
	cout << "Done" << endl;

	cin.ignore();
}
#pragma endregion
